import numpy as np
import os, sys, glob
from parse import load_ps
import matplotlib.pyplot as plt
from scipy.signal import fftconvolve
import warnings
warnings.filterwarnings('ignore')

def find(condition):
    res, = np.nonzero(np.ravel(condition))
    return res

def keypointsToConsider(exercise_type, side):
    keypoints = []
    if exercise_type == 'bicep_curl':
        keypoints = [side+'wrist', side+'elbow', side+'shoulder']
    
    return keypoints

def exerciseStatus(points, parts_to_checked, threshold_matrix):
    flag = True
    for i in range(len(points)):
        point = points[i]
        if point[0] > threshold_matrix[i][0] or point[1] < threshold_matrix[i][1]:
            flag = False
    return flag

def extractValues(frame, keypoints_needed):
    vals_str = frame.Print(keypoints_needed)
    vals = vals_str.split('\n')[:-1]
    keypoints_coordinates = [0]*len(vals)
    for i in range(len(vals)):
        keypoints_coordinates[i] = vals[i].split(':')[1].split(',')
        keypoints_coordinates[i] = [float(j) for j in keypoints_coordinates[i]]
        keypoints_coordinates[i] = (keypoints_coordinates[i][0], keypoints_coordinates[i][1])
    return keypoints_coordinates
def getKeypointStats(numpy_file, part_names):
    file = load_ps(numpy_file)
    poses = file.poses
    keyps = keypointsExtraction(poses, part_names)
    keyps_stats = extractDiffExtremeVals(keyps, part_names)
    return keyps, keyps_stats
def keypointsExtraction(poses, part_names):
    keyps = [0]*len(poses)
    prev_vals = [(0,0)]*len(part_names)
    for i in range(len(poses)):
        pose = poses[i]
        vals = []
        for j in range(len(part_names)):
            part = part_names[j]
            
            if getattr(pose, part).exists == False:
                val = prev_vals[j]
            else:
                val = (getattr(pose, part).x, getattr(pose, part).y)
                prev_vals[j] = val
            vals.append(val)
        
        keyps[i] = vals
    return np.array(keyps)
def difference(a, b):
    x_diff = abs(a[0] - b[0])
    y_diff = abs(a[1] - b[1])
    diff = np.sqrt(x_diff**2 + y_diff**2)
    return diff
def extractDiffExtremeVals(keyps, part_names):
    diff = {}
    keyps_marginal = None
    for i in range(len(part_names)):
        keyps_marginal = keyps[:,i,:]
        keyps_marginal[:,0] = (keyps_marginal[:,0] - np.mean(keyps_marginal[:,0]))/np.std(keyps_marginal[:,0])
        keyps_marginal[:,1] = (keyps_marginal[:,1] - np.mean(keyps_marginal[:,1]))/np.std(keyps_marginal[:,1])
        diff_list = []
        part = part_names[i]
        for j in range(1, len(keyps_marginal)):
            diff_list.append(difference(keyps_marginal[j], keyps_marginal[j-1]))
        stats = [min(diff_list), max(diff_list), np.argmin(diff_list)+1, np.argmax(diff_list)+1]
        diff.update({part:stats})
    return diff
def extractMarginalValues(keypoints, idx):
    marginal = [a[idx] for a in keypoints]
    return np.array(marginal) # dimensions : 2

def plotData(data):
    plt.plot(np.arange(len(data)), data, '-')
    plt.show()
    
def plotMovingPeriodUsingAC(data):
    periodicity = []
#     started = False
    for i in range(len(data)):
        period = getPeriodUsingAutocorr(data[:i])
#         if started == True:
#             periodicity.append(period)
#         else:
#             if period != -1:
        periodicity.append(period)

    plt.plot(np.arange(len(periodicity)), periodicity)
    plt.show()
    
def getStartAndEndPoint(x, min_diff_threshold, max_diff_threshold, required_length):
    startFound, endFound, startIdx, lengthOfSeq = False, False, -1, 0
    prev_period = -1
    temp = 0
    i = 0
    while i < len(x):
        # print(i)
        if x[i] != -1 and startFound == False:
            startFound = True
            startIdx = i
            temp = i
            prev_period = x[i]
            lengthOfSeq = 1
        elif x[i] != -1:
            # print(abs(prev_period - x[i]))
            if ((abs(prev_period-x[i]) > max_diff_threshold) or (abs(prev_period-x[i]) < min_diff_threshold)) and (lengthOfSeq < required_length):
                temp = i
                prev_period = x[i]
                startIdx = i
                lengthOfSeq = 1
            elif (abs(prev_period-x[i]) <= max_diff_threshold) and (abs(prev_period-x[i]) >= min_diff_threshold):
                temp = i
                lengthOfSeq += 1
                prev_period = x[i]
            elif (abs(prev_period-x[i]) > max_diff_threshold or (abs(prev_period-x[i]) < min_diff_threshold)) and lengthOfSeq >= required_length:
                endFound = True
                break
        elif x[i] == -1 and startFound == True:
            if lengthOfSeq >= required_length:
                endFound = True
                break
            else:
                startFound = False
                lengthOfSeq = 0
        i += 1
    # print(startIdx, x[startIdx])
    return startFound, endFound, max(int(startIdx-x[startIdx]), 1), i

def processMovingPeriodicityAndGetEndPoints(data, min_diff_threshold, max_diff_threshold, required_length):
    moving_period_vals = []
    for i in range(len(data)):
        moving_period_vals.append(getPeriodUsingAutocorr(data[:i]))
        
    startFound, endFound, startIdx, endIdx = getStartAndEndPoint(moving_period_vals, min_diff_threshold, max_diff_threshold, required_length)
    return startFound, endFound, startIdx, endIdx

def getPeriodUsingZeroCrossings(data):
    indices = find((data[1:] >=np.mean(data) ) & (data[:-1] < np.mean(data)))
    crossings = [i-data[i] / (data[i+1] - data[i]) for i in indices]
    return np.mean(np.diff(crossings))
    
def getPeriodUsingAutocorr(data):
    corr = fftconvolve(data, data[::-1], mode='full')
    corr = corr[len(corr)//2:]
    # Find the first low point
#     print(corr)
    d = np.diff(corr)
#     print("printing d",d)
    temp = find(d > 0)
    if len(temp) > 0:
        start = find(d > 0)[0]
#         print(start)
        # Find the next peak after the low point (other than 0 lag).  This bit is
#         print(np.argmax(corr[start:]))
        peak = np.argmax(corr[start:]) + start
#         print(peak)
        if peak < len(corr)-1:
            px, py = parabolic(corr, peak)
            return px
        else:
            return -1
    else:
        return -1

def parabolic(f, x):
    """Quadratic interpolation for estimating the true position of an
    inter-sample maximum when nearby samples are known.

    f is a vector and x is an index for that vector.

    Returns (vx, vy), the coordinates of the vertex of a parabola that goes
    through point x and its two neighbors.

    Example:
    Defining a vector f with a local maximum at index 3 (= 6), find local
    maximum if points 2, 3, and 4 actually defined a parabola.

    In [3]: f = [2, 3, 1, 6, 4, 2, 3, 1]

    In [4]: parabolic(f, argmax(f))
    Out[4]: (3.2142857142857144, 6.1607142857142856)

    """
#     print('printing f', f, x)
#     print('numerator', f[x-1])
#     print('numerator', f[x+1])
    xv = 1/2. * (f[x-1] - f[x+1]) / (f[x-1] - 2 * f[x] + f[x+1]) + x
    yv = f[x] - 1/4. * (f[x-1] - f[x+1]) * (xv - x)
    return (xv, yv)