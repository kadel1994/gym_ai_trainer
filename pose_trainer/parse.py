import argparse
import glob
import json
import numpy as np
import os
import natsort

from pose import Pose, Part, PoseSequence
from pprint import pprint


def main():

    parser = argparse.ArgumentParser(description='Pose Trainer Parser')
    parser.add_argument('--input_folder', type=str, default='poses', help='input folder for json files')
    parser.add_argument('--output_folder', type=str, default='poses_compressed', help='output folder for npy files')
    
    args = parser.parse_args()

    video_paths = glob.glob(os.path.join(args.input_folder, '*'))
    video_paths = sorted(video_paths)

    # Get all the json sequences for each video
    all_ps = []
    for video_path in video_paths:
        all_ps.append(parse_sequence(video_path, args.output_folder))
    return video_paths, all_ps

def sortedOrdering(keypoints):
    PART_NAMES = ['Nose', 'Neck',  'RShoulder', 'RElbow', 'RWrist', 'LShoulder', 'LElbow', 'LWrist', 'RHip', 'RKnee', 'RAnkle', 'LHip', 'LKnee', 'LAnkle', 'REye', 'LEye', 'REar', 'LEar']
    new_keyps = []
    for part in PART_NAMES:
        if part in list(keypoints.keys()):
            indices = keypoints[part]
            indices.append(1)
            new_keyps.append(indices)
        else:
            new_keyps.append([-1,-1,0])
    return new_keyps

def parse_sequence(json_folder, output_folder):
    """Parse a sequence of OpenPose JSON frames and saves a corresponding numpy file.

    Args:
        json_folder: path to the folder containing OpenPose JSON for one video.
        output_folder: path to save the numpy array files of keypoints.

    """
    json_files = glob.glob(os.path.join(json_folder, '*.json'))
    json_files = natsort.natsorted(json_files)

    num_frames = len(json_files)
    all_keypoints = []
    for i in range(num_frames):
        with open(json_files[i]) as f:
            json_obj = json.load(f)
            if json_obj != []:
                keypoints = json_obj[0]
                keypoints = sortedOrdering(keypoints)
                all_keypoints.append(keypoints)
            # all_keypoints[i] = keypoints.reshape((18, 3))
            # all_keypoints = [[a,b,c]*18, [a,b,c]*18 .......]
    
    output_dir = os.path.join(output_folder, os.path.basename(json_folder))
    np.save(output_dir, all_keypoints)


def load_ps(filename):
    """Load a PoseSequence object from a given numpy file.

    Args:
        filename: file name of the numpy file containing keypoints.
    
    Returns:
        PoseSequence object with normalized joint keypoints.
    """
    all_keypoints = np.load(filename)
    return PoseSequence(all_keypoints)


if __name__ == '__main__':
    main()
