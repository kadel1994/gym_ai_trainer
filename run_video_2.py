import argparse
import logging
import time

import cv2
import json
import numpy as np

import subprocess
import os

from tf_pose.estimator import TfPoseEstimator
from tf_pose.networks import get_graph_path, model_wh

import sys
sys.path.append('pose_trainer')

from parse import parse_sequence, load_ps
from evaluate import evaluate_pose

# from object_recognition.yolo_utils import infer_image, show_image

logger = logging.getLogger('TfPoseEstimator-Video')
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('[%(asctime)s] [%(name)s] [%(levelname)s] %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

fps_time = 0


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='tf-pose-estimation Video')
    parser.add_argument('--video', type=str, default='')
    parser.add_argument('--resolution', type=str, default='432x368', help='network input resolution. default=432x368')
    parser.add_argument('--model', type=str, default='mobilenet_thin', help='cmu / mobilenet_thin / mobilenet_v2_large / mobilenet_v2_small')
    parser.add_argument('--show-process', type=bool, default=False,
                        help='for debug purpose, if enabled, speed for inference is dropped.')
    parser.add_argument('--showBG', type=bool, default=True, help='False to show skeleton only.')
    parser.add_argument('--output_dir', type=str, default='./outputs_json/video/')
    parser.add_argument('--resize-out-ratio', type=float, default=2.0,
                        help='if provided, resize heatmaps before they are post-processed. default=1.0')
    parser.add_argument('--exercise', type=str, default='bicep_curl')
    parser.add_argument('--config', type=str, default='./object_recognition/yolov3-coco/yolov2.cfg')
    parser.add_argument('--weights', type=str, default='./object_recognition/yolov3-coco/yolov2.weights')
    parser.add_argument('--confidence',type=float,default=0.5,help='to reject boundaries')
    parser.add_argument('--threshold',type=float,default=0.3,help='threshold for non max suppression')
    parser.add_argument('--show-time',type=bool,default=False,help='show time')

    args = parser.parse_args()

    logger.debug('initialization %s : %s' % (args.model, get_graph_path(args.model)))
    w, h = model_wh(args.resolution)
    e = TfPoseEstimator(get_graph_path(args.model), target_size=(w, h))
    cap = cv2.VideoCapture(args.video)

    if cap.isOpened() is False:
        print("Error opening video stream or file")

    video_name = args.video.split('/')[-1]
    video_name = video_name.split('.')[0]

    ret_val, image = cap.read()

    # # some preprocessing for object recognition
    # labels = open('./object_recognition/yolov3-coco/coco-labels').read().strip().split('\n')
    # # Intializing colors to represent each label uniquely
    # colors = np.random.randint(0, 255, size=(len(labels), 3), dtype='uint8')
    # # Load the weights and configutation to form the pretrained YOLOv3 model
    # net = cv2.dnn.readNetFromDarknet(args.config, args.weights)
    # # Get the output layer names of the model
    # layer_names = net.getLayerNames()
    # layer_names = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]
    # # initialize objects
    # height, width = None, None
    # writer = None

    frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    cc = 0

    while True and cc <= frame_count-10:
        ret_val, image = cap.read()
        cc += 1

        # ### object detection code
        # if width is None or height is None:
        #     height, width = image.shape[:2]

        # frame, _, _, _, _ = infer_image(net, layer_names, height, width, image, colors, labels, args)
        # cv2.namedWindow('YOLO object detection', cv2.WINDOW_NORMAL)
        # cv2.imshow('YOLO object detection', frame)

        ### pose estimation
        humans = e.inference(image, resize_to_default=(w > 0 and h > 0), upsample_size=args.resize_out_ratio)
        if not args.showBG:
            image = np.zeros(image.shape)
        image, body_parts_all_humans = TfPoseEstimator.draw_humans(image, humans, imgcopy=False)
        # saving the keypoints co-ordinates as a dict object
    
        frame_name = "frame" + str(cc)
        frame_path = args.output_dir+video_name+"/"+frame_name+".json"

        if not os.path.exists(args.output_dir+video_name+"/"):
            os.makedirs(args.output_dir+video_name+"/")

        with open(frame_path, "w") as f:
            json.dump(body_parts_all_humans, f)

        cv2.putText(image, "FPS: %f" % (1.0 / (time.time() - fps_time)), (10, 10),  cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
        cv2.namedWindow('tf-pose-estimation result', cv2.WINDOW_NORMAL)
        
        cv2.imshow('tf-pose-estimation result', image)
        fps_time = time.time()
        if cv2.waitKey(1) == 27:
            break
        

    cv2.destroyAllWindows()
    print("all the windows are destroyed")

    # reading all the json frames
    parse_sequence(args.output_dir+video_name, 'outputs_npy/')
    pose_seq = load_ps('outputs_npy/' + video_name + '.npy')
    (correct, feedback) = evaluate_pose(pose_seq, args.exercise)
    if correct:
        print('Exercise performed correctly!')
    else:
        print('Exercise could be improved:')
    print(feedback)

logger.debug('finished+')
